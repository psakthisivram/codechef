package com.siv.codechef;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class CookOffContest {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Set<String> input = new HashSet<String>();
		Set<String> comboList = new HashSet<String>(Arrays.asList("easy", "medium", "medium-hard1",	"simple", "cakewalk"));
		
		int T = scan.nextInt();
		
		while(T-- >0) {
			int N = scan.nextInt();
			while(N-->0) {
				input.add(scan.next());
			}
		}
		System.out.println(comboList);
		System.out.println(input);
		System.out.println(comboList.equals(input));
	}
}
