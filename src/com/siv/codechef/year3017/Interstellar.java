package com.siv.codechef.year3017;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Interstellar {
	
	private static String findShortPath(Query query) {
		int distance = 0;
		System.out.println(query);
		
		Universe universe_1 = query.getUniverse_1();
		Planet planet_1 = query.getPlanet_1();
		
		Universe universe_2 = query.getUniverse_2();
		Planet planet_2 = query.getPlanet_2();
		
		if(universe_1 != universe_2) {
			System.out.println("1 : "+universe_1.getTeleportMap());
			if(universe_1.getTeleportMap().containsKey(planet_1.getId())) {
				System.out.println("2");
				Set<Teleport> teleports_1 = universe_1.getTeleportMap().get(planet_1.getId()); 
				for(Teleport teleport : teleports_1) {
					System.out.println("3");
					if(teleport.getUniverse().getId() == universe_2.getId()) {
						System.out.println("4");
						if(teleport.getPlanet().getId() == planet_2.getId()) {
							System.out.println("All Pass !");
							distance = 1;
							break;
						}
					}
				}
			}
		}
		
		if(distance == 0)
			return "impossible";
		else
			return String.valueOf(distance);
	}
	
	public static void main(String[] args) {
		/*3 3 3
		1 2
		2 3
		1 1 1 2
		3 3 3 2
		1 2 3 3
		2 1 2 2
		2 1 2 3
		1 2 3 2*/
		Scanner scan = new Scanner(System.in);
		String[] input = scan.nextLine().split(" ");
		
		// Parse inputs
		int planetCount = Integer.parseInt(input[0]);
		int teleportCount = Integer.parseInt(input[1]);
		int queryCount = Integer.parseInt(input[2]);
				
		Map<Integer, Universe> universes = new HashMap<>();
		Map<Integer, Planet> planets = new HashMap<>();
		
		// Creating Planets
		for(int i = 1; i <= planetCount; i++) {
			planets.put(i, new Planet(i));
		}
		
		// Adding Tunnels
		for(int i = 0; i < planetCount-1; i++) {
			String[] tunnel = scan.nextLine().split(" ");
			Planet planet1 = planets.get(Integer.parseInt(tunnel[0]));
			Planet planet2 = planets.get(Integer.parseInt(tunnel[1]));
			
			planet1.addTunnel(planet2);
			planet2.addTunnel(planet1);
		}	
		
		// Adding Teleports
		for(int i = 0; i < teleportCount; i++) {
			String[] teleportInput = scan.nextLine().split(" ");
			
			Universe universe_1 = null;
			if(!universes.containsKey(Integer.parseInt(teleportInput[1]))){
				universes.put(Integer.parseInt(teleportInput[1]), new Universe(Integer.parseInt(teleportInput[1]), new HashMap<>(planets)));				
			}
			universe_1 = universes.get(Integer.parseInt(teleportInput[1]));
			
			Universe universe_2 = null;
			if(!universes.containsKey(Integer.parseInt(teleportInput[3]))){
				universes.put(Integer.parseInt(teleportInput[3]), new Universe(Integer.parseInt(teleportInput[3]), new HashMap<>(planets)));				
			}
			universe_2 = universes.get(Integer.parseInt(teleportInput[3]));									
			
			Planet planet_universe_2 = universe_2.getPlanets().get(Integer.parseInt(teleportInput[2]));
			universe_1.addTeleport(Integer.parseInt(teleportInput[0]), new Teleport(planet_universe_2, universe_2));
			
			Planet planet_universe_1 = universe_1.getPlanets().get(Integer.parseInt(teleportInput[0]));
			universe_2.addTeleport(Integer.parseInt(teleportInput[2]), new Teleport(planet_universe_1, universe_1));		
		}
				
		// Print Universe
		for(Universe universe : universes.values()) {
			System.out.println("Universe "+universe.getId()+" has "+universe.getPlanets().size()+" planets. Teleports are "+universe.getTeleportMap());
			for(Planet planet : universe.getPlanets().values()) {				
				System.out.println("\t"+planet+" has "+planet.getTunnel()+" tunnels.");				
			}
		}
		
		Query[] queries = new Query[queryCount];
		for(int i = 0; i< queryCount; i++) {
			String[] query = scan.nextLine().split(" ");
			queries[i] = new Query(planets.get(Integer.parseInt(query[0])), universes.get(Integer.parseInt(query[1])), 
												planets.get(Integer.parseInt(query[2])), universes.get(Integer.parseInt(query[3])));
		}
		
		System.out.println(findShortPath(queries[0]));
	}

}
