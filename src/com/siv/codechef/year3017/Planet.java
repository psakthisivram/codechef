package com.siv.codechef.year3017;

import java.util.HashSet;
import java.util.Set;

public class Planet {
	
	private int id;
	private Set<Planet> tunnel = new HashSet<>();
	public String name;
	
	public Planet(int id) {
		super();
		this.id = id;
	}

	public Planet(int id, Set<Planet> tunnel) {
		this.id = id;
		this.tunnel = tunnel;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void addTunnel(Planet planet) {
		tunnel.add(planet);
	}
	
	public boolean hasTunnel(Planet planet) {
		return tunnel.contains(planet);
	}	

	public Set<Planet> getTunnel() {
		return tunnel;
	}

	@Override
	public String toString() {
		return "Planet [id=" + id + "]";
	}
}
