package com.siv.codechef.year3017;

public class Query {
	private Planet planet_1;
	private Universe universe_1;
	private Planet planet_2;
	private Universe universe_2;
	
	public Query(Planet planet_1, Universe universe_1, Planet planet_2, Universe universe_2) {
		super();
		this.planet_1 = planet_1;
		this.universe_1 = universe_1;
		this.planet_2 = planet_2;
		this.universe_2 = universe_2;
	}

	public void setPlanet_1(Planet planet_1) {
		this.planet_1 = planet_1;
	}

	public void setUniverse_1(Universe universe_1) {
		this.universe_1 = universe_1;
	}

	public void setPlanet_2(Planet planet_2) {
		this.planet_2 = planet_2;
	}

	public void setUniverse_2(Universe universe_2) {
		this.universe_2 = universe_2;
	}

	public Planet getPlanet_1() {
		return planet_1;
	}

	public Universe getUniverse_1() {
		return universe_1;
	}

	public Planet getPlanet_2() {
		return planet_2;
	}

	public Universe getUniverse_2() {
		return universe_2;
	}

	@Override
	public String toString() {
		return "Query [planet_1=" + planet_1.getId() + ", universe_1=" + universe_1.getId() + ", planet_2=" + planet_2.getId() + ", universe_2="
				+ universe_2.getId() + "]";
	}
	
	
}
