package com.siv.codechef.year3017;

import java.util.Comparator;

public class Teleport implements Comparable<Teleport>{
	private Planet planet;
	private Universe universe;

	public Teleport(Planet planet, Universe universe) {
		super();
		this.planet = planet;
		this.universe = universe;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public Universe getUniverse() {
		return universe;
	}

	public void setUniverse(Universe universe) {
		this.universe = universe;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((planet == null) ? 0 : planet.getId());
		result = prime * result + ((universe == null) ? 0 : universe.getId());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teleport other = (Teleport) obj;
		if (planet == null) {
			if (other.planet != null)
				return false;
		}
		if (universe == null) {
			if (other.universe != null)
				return false;
		}  
		if (planet.getId() != other.planet.getId() || universe.getId() != other.universe.getId())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Teleport [planet=" + planet.getId() + ", universe=" + universe.getId() + "]";
	}
	
	static class CompareByPlanetIdUniverseId implements Comparator<Teleport> {

		@Override
		public int compare(Teleport o1, Teleport o2) {
			return Integer.compare(Integer.compare(o1.planet.getId(), o2.planet.getId()), Integer.compare(o1.universe.getId(), o2.getUniverse().getId()));
		}
		
	}

	@Override
	public int compareTo(Teleport o) {
		return new CompareByPlanetIdUniverseId().compare(this, o);
	}

}
