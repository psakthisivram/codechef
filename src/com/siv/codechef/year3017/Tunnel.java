package com.siv.codechef.year3017;

public class Tunnel {
	private Planet planet;

	public Tunnel(Planet planet) {
		super();
		this.planet = planet;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}
	
}
