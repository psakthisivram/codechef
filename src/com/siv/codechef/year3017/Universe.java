package com.siv.codechef.year3017;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Universe implements Comparable<Integer> {
	private int id;
	private Map<Integer, Planet> planets;
	private Map<Integer, Set<Teleport>> teleportMap = new HashMap<>();
	
	public Universe(int id) {
		super();
		this.id = id;
	}
	
	public Universe(int id, Map<Integer, Planet> planets) {
		super();
		this.id = id;
		this.planets = planets;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Map<Integer, Planet> getPlanets() {
		return planets;
	}

	public void setPlanets(Map<Integer, Planet> planets) {
		this.planets = planets;
	}
	
	public void addPlanet(int id, Planet planet) {
		planets.put(id, planet);
	}

	public void addTeleport(int id, Teleport teleport) {
		Set<Teleport> teleports = null;
		if(this.teleportMap.containsKey(id)) {
			teleports = this.teleportMap.get(id);
			teleports.add(teleport);
		} else {
			teleports = new HashSet<>();
			teleports.add(teleport);
			teleportMap.put(id, teleports);
		}
	}
	
	public Map<Integer, Set<Teleport>> getTeleportMap() {
		return teleportMap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Universe other = (Universe) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Universe [id=" + id + "]";
	}
	
	@Override
	public int compareTo(Integer o) {
		return Integer.compare(this.id, o);
	}
	
}
